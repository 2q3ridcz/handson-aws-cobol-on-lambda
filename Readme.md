# aws-cobol-on-lambda

[AWS LambdaのCustom RuntimeでCOBOLを動かしてみた #reinvent | DevelopersIO](https://dev.classmethod.jp/articles/lambda-custom-runtimes-cobol/)

これをやる。

## 概要

lambdaにデプロイするのは Zipファイル。
Zipには以下のファイルを含める。
- コンパイルした Cobolオブジェクト
- bootstrap (lambda実行用の設定？)
- ライブラリたち

コンパイル＆Zip圧縮は amazonlinux:1 のコンテナで実施。そのため、Dockerを使える環境が必要。

冒頭のサイトは EC2の Amazon Linuxに Docker環境を構築したが、自分は Windows10 PCに Docker環境があるのでそれを使用。

## Coding

Dockerfile、bootstrap、hello.cob の各ファイルは冒頭のサイトどおりに作成する。

build.sh は Linux用。自分は Windows なので build.ps1 ファイルを自作する。中身はほぼ同じで、 `` `pwd` `` の部分が違うくらい。

build.ps1

```powershell
docker build -t lambda-cobol:latest .
docker run -it --rm -v ${PWD}:/tmp lambda-cobol cp /lambda-cobol-hello.zip /tmp/
```

## Build

あとは build.ps1 を実行するだけ。build.ps1がコンテナを実行し、lambda-cobol-hello.zipを作成する。

```powershell
./build.ps1
```

## Deploy

- ★前提★
    1. AWS CLIを使用するための Access Key ID等を用意してあること。  
    ([Configuration basics - AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) 参照。)
    2. Lamda関数の権限を持った Roleを用意し、その ARNがわかること。  
    ([Amazon リソースネーム (ARN) - AWS 全般のリファレンス](https://docs.aws.amazon.com/ja_jp/general/latest/gr/aws-arns-and-namespaces.html) 参照。)

冒頭のサイトのとおり、AWS CLIを使う。自分の PCにインストールしていないので、コンテナ内で実行する。

まずはコンテナを起動し、コンテナ内に入る。

```powershell
docker run -it --rm --name aws-sam-cli `
    -v ${PWD}:/tmp `
    amazon/aws-sam-cli-build-image-provided `
    /bin/bash
```

次に、コンテナ内で Lambda関数を作成コマンドを実行する。流れはこう。

1. 最初に AWS CLIをセットアップ。
2. Lambda関数用 Roleを設定する。  
    "arn:aws:iam::[account-id]:role/service-role/[role-name]" という形式。  
    [account-id] は 自身の AWS アカウントの ID (12桁の数字)。  
    [role-name] は Lambda関数の実行権限を持つロールの名前。
3. Lambda関数を作成する。

```bash
# 1. Configure aws-cli.
aws configure
# Answer values like these...
# AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
# AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
# Default region name [None]: ap-northeast-1
# Default output format [None]: json

# 2. Setup lambda function role
# "123456789012" should be replaced to your account-id.
# "exe-lambda" should be replaced to your role-name.
export LAMBDA_EXEC_ROLE=arn:aws:iam::123456789012:role/service-role/exe-lambda

# 3. Create function
cd /tmp
aws lambda create-function \
    --function-name hello-cobol \
    --runtime provided \
    --role $LAMBDA_EXEC_ROLE \
    --handler cobol.hello \
    --zip-file fileb://lambda-cobol-hello.zip \
    --region ap-northeast-1
```

ここで `exit` してもいいが、以降も AWS CLIを使いたい場合はまだ `exit` しないでおく。


## Test

冒頭のサイトと同じように AWSマネジメントコンソールの Lambda画面からテストするか、AWS CLIでテストする方法がある。

まずは、AWSマネジメントコンソールの Lambda画面でテスト実行。

![test-result.png](./img/test-result.png)

次に、AWS CLIでテスト実行。  
Deployの作業の流れで以下を実行すると「Hello world!」と表示される。  
※Deploy後に `exit` してしまった場合は、Deploy時と同じように AWS CLIのコンテナに入って `aws configure` してから以下を実行。

```bash
# 4. test
cd /tmp
aws lambda invoke --function-name hello-cobol result.txt

cat result.txt
```

## 後片付け

AWSマネジメントコンソールの Lambda画面から削除を実行。

![clean-up.png](./img/clean-up.png)

## おまけ_BuildOnLinuxContainer

今回は不要な作業だったが、備忘のため残す。

冒頭のサイトは build.sh を実行する手順になっているので、Dockerをインストールした Linux環境が必要だと思った。(最終的には、build.sh を元に build.ps1を作成して実行する方が楽だと気づいた…)

色々調べた結果、Docker-in-Dockerの例が 
[Docker - Official Image | Docker Hub](https://hub.docker.com/_/docker/) に書いてあった。

Docker-in-Dockerで Buildする手順は以下。

```powershell
# コンテナを起動する。
docker run --privileged --name some-docker -d `
    -e DOCKER_TLS_CERTDIR=/certs `
    -v some-docker-certs-ca:/certs/ca `
    -v some-docker-certs-client:/certs/client `
    -v ${PWD}:/tmp `
    docker:dind

# コンテナ内のシェルに接続する。
docker exec -it some-docker /bin/sh
```

コンテナ内で build.shを実行する。コンテナ内に bashはないため、事前に build.shの 1行目を `#!/bin/sh` に変更してある。

```sh
cd /tmp
./build.sh
exit
```

これで、lambda-cobol-hello.zipが作成されたはず。コンテナは不要なので削除する。

```powershell
docker container stop some-docker
docker rm some-docker
```
